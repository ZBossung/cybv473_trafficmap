'''
Track and visualize TCP and UDP connections
Zack Bossung
May 2020
Version 2.0

NOTES:

1) Requires Python 3
2) You must install
  2a) prettytable (pip install PrettyTable)
  2b) scapy (pip install scapy)
  2c) gmplot (pip install gmplot)
  2d) ip2location (pip install IP2Location)
3) You must have a Google Maps JavaScript API key
  3a) instructions to aqcuire one: https://developers.google.com/maps/documentation/javascript/get-api-key
  (My) API key: https://console.cloud.google.com/apis/credentials?project=zbossungcybv473finalproject&folder=&organizationId=
4) Keep .bin database file in same directory as script

Explanation:

This script is an illustration of what i have learned thoughout the semester. It
accepts arguments via the argparse module, uses scapy to gather network traffic,
then parses that network traffic and prints a pretty table displaying what kind of 
traffic is is, where is is coming from/going to and exports a .html google map visualizing
the traffic.

'''

##Import Python librarys
import argparse
from prettytable import PrettyTable
from scapy.all import *
import gmplot
import IP2Location
import requests
import json

print("\nWelcome to the WiFi Sniffer Version 2.0\n")

print("BossungZ_FinalProject.py\nZack Bossung\nMay 2020\n")

# Create a parser object 
parser = argparse.ArgumentParser('Enter the WI-FI Adapter to Monitor')

# Add possible arguments to the parser object
parser.add_argument('-v', "--verbose", help="dictates the level of output detail", action='store_true')
parser.add_argument('-i', '--interface',  required=False, default='Wi-Fi', help="specify the network interface to monitor. Default: Wi-Fi")
parser.add_argument('-t', '--timeout', required=False, default=10, help="specify the amount of time to sniff packets. Default: 10")
parser.add_argument('-o', '--output', required=False, default='.', help="Specify the output directory to export output files. Default: directory script is being ran from (.\)")
parser.add_argument('-a', '--apikey', required=True, help="Specify your Google Maps JavaScript API key")

# Process the arguments
args = parser.parse_args()

VERBOSE = args.verbose
NET_INTERFACE = args.interface
TIMEOUT = int(args.timeout)
OUTPUT = args.output
APIKEY = args.apikey

# Set the verbose status
if args.verbose:
    VERBOSE = True
    print("Attempting to Sniff Wifi Packets\n")
    print("++++Arguments used++++")
    print("Verbose: ", VERBOSE)
    print("WiFi Interface: ", NET_INTERFACE)
    print("Capture timeout: ", TIMEOUT)
    print("Output path: ", OUTPUT)
    print("APIKey: ", APIKEY)
else:
    VERBOSE = False
    
# Configure the SCAPY interface
conf.iface = NET_INTERFACE

# Capture packets
print("\nStarting packet capture. Timeout: ", TIMEOUT)
pktCapture = sniff(timeout=TIMEOUT)

print("Finished capturing packets: ", pktCapture, "\nProcessing...\n")

# Parse through packets creating dictionaries for TCP and UDP packets respectively
tcpDict = {}
udpDict = {}

for pkt in pktCapture:
    if pkt.haslayer(TCP):
        tcpKey = str("TCP {} {} {} {}".format(pkt[IP].src, pkt[IP].dst, pkt[TCP].sport, pkt[TCP].dport))
        if tcpKey in tcpDict.keys():
            occur = tcpDict[(tcpKey)]
            occur += 1
            tcpDict[(tcpKey)] = occur
        else:
            tcpDict[(tcpKey)] = 1
    elif pkt.haslayer(UDP):
        udpKey = str("UDP {} {} {} {}".format(pkt[IP].src, pkt[IP].dst, pkt[UDP].sport, pkt[UDP].dport))
        if udpKey in udpDict.keys():
            occur = udpDict[(udpKey)]
            occur += 1
            udpDict[(udpKey)] = occur
        else:
            udpDict[(udpKey)] = 1

# Get local ip and public ip addresses
localIPv4Address = get_if_addr(ifaces.dev_from_name(NET_INTERFACE))
publicIPv4Address = requests.get('https://ipinfo.io/json').json()['ip']

# Initialize PrettyTable
pTable = PrettyTable()
pTable.field_names = ["Protocol", "Source IP", "Destination IP", "Source Port", "Destination Port", "Source Location", "Destination Location", "Occurences"]

# Initialize IP2Location
ipLocObj = IP2Location.IP2Location(".\\IP2LOCATION-LITE-DB11.BIN")

# Initialize gmplot
gmap = gmplot.GoogleMapPlotter(30, 0, 3)

# Add each directory entry to PrettyTable, map each lattitude and longitude location for source and destination addresses
for tcpEntry in tcpDict:
    tcpKeyEntry = str(tcpEntry).split(' ')
    
    # Replace local interface IP address with public facing IP address
    if tcpKeyEntry[1] == localIPv4Address:
        srcLocation = ipLocObj.get_city(publicIPv4Address) + ", " + ipLocObj.get_region(publicIPv4Address) + ", " + ipLocObj.get_country_long(publicIPv4Address)
        dstLocation = ipLocObj.get_city(tcpKeyEntry[2]) + ", " + ipLocObj.get_region(tcpKeyEntry[2]) + ", " + ipLocObj.get_country_long(tcpKeyEntry[2])
        pTable.add_row([tcpKeyEntry[0], tcpKeyEntry[1], tcpKeyEntry[2], tcpKeyEntry[3], tcpKeyEntry[4], srcLocation, dstLocation, tcpDict[tcpEntry]])
        tcpKeyEntry[1] = publicIPv4Address
    
    if tcpKeyEntry[2] == localIPv4Address:
        srcLocation = ipLocObj.get_city(tcpKeyEntry[1]) + ", " + ipLocObj.get_region(tcpKeyEntry[1]) + ", " + ipLocObj.get_country_long(tcpKeyEntry[1])
        dstLocation = ipLocObj.get_city(publicIPv4Address) + ", " + ipLocObj.get_region(publicIPv4Address) + ", " + ipLocObj.get_country_long(publicIPv4Address)
        pTable.add_row([tcpKeyEntry[0], tcpKeyEntry[1], tcpKeyEntry[2], tcpKeyEntry[3], tcpKeyEntry[4], srcLocation, dstLocation, tcpDict[tcpEntry]])
        tcpKeyEntry[2] = publicIPv4Address
    
    # Create lat and lon lists containing source and destination locations   
    tcpLatList = [ipLocObj.get_latitude(tcpKeyEntry[1]), ipLocObj.get_latitude(tcpKeyEntry[2])]
    tcpLonList = [ipLocObj.get_longitude(tcpKeyEntry[1]), ipLocObj.get_longitude(tcpKeyEntry[2])]
    
    # Plot each value in lat and lon lists color blue if TCP
    gmap.plot(tcpLatList, tcpLonList, 'cornflowerblue', edge_width=5)
    
for udpEntry in udpDict:
    udpKeyEntry = str(udpEntry).split(' ')
    
    if udpKeyEntry[1] == localIPv4Address:
        srcLocation = ipLocObj.get_city(publicIPv4Address) + ", " + ipLocObj.get_region(publicIPv4Address) + ", " + ipLocObj.get_country_long(publicIPv4Address)
        dstLocation = ipLocObj.get_city(udpKeyEntry[2]) + ", " + ipLocObj.get_region(udpKeyEntry[2]) + ", " + ipLocObj.get_country_long(udpKeyEntry[2])
        pTable.add_row([udpKeyEntry[0], udpKeyEntry[1], udpKeyEntry[2], udpKeyEntry[3], udpKeyEntry[4], srcLocation, dstLocation, tcpDict[tcpEntry]])        
        udpKeyEntry[1] = publicIPv4Address
    
    if udpKeyEntry[2] == localIPv4Address:
        srcLocation = ipLocObj.get_city(udpKeyEntry[1]) + ", " + ipLocObj.get_region(udpKeyEntry[1]) + ", " + ipLocObj.get_country_long(udpKeyEntry[1])
        dstLocation = ipLocObj.get_city(publicIPv4Address) + ", " + ipLocObj.get_region(publicIPv4Address) + ", " + ipLocObj.get_country_long(publicIPv4Address)
        pTable.add_row([udpKeyEntry[0], udpKeyEntry[1], udpKeyEntry[2], udpKeyEntry[3], udpKeyEntry[4], srcLocation, dstLocation, tcpDict[tcpEntry]])
        udpKeyEntry[2] = publicIPv4Address
        
    udpLatList = [ipLocObj.get_latitude(udpKeyEntry[1]), ipLocObj.get_latitude(udpKeyEntry[2])]
    udpLonList = [ipLocObj.get_longitude(udpKeyEntry[1]), ipLocObj.get_longitude(udpKeyEntry[2])]
    
    gmap.plot(udpLatList, udpLonList, 'darkorchid', edge_width=5)
    
# Specify Google Maps JavaScript API key
gmap.apikey = APIKEY

# Output map
gmap.draw(OUTPUT + '\GMap.html')

# Output PrettyTable to screen and file
with open('pTable.txt', 'w') as out:
    print(pTable, file=out)
print(pTable)

'''
Need to:
Correct router address to public IP (random udp leg going off into the pacific)
'''